import React from 'react';
import './screen.css';

export default function Screen (props) {
  return (
    <div>
      <output class='text'>{props.text}</output>
      <output class='total'>{props.total}</output>
    </div>
  );
}

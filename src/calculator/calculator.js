import React from 'react';
import Button from './button';
import Screen from './screen';
import * as computer from './computer';

export default class Calculator extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      input: '',
      computedValue: null,
      priorizedComputedValue: null,
      inputValue: null,
      currentOperandFunc: null,
      canUseOperand: false
    };
  }

  clickOnNumber (number) {
    this.setState({
      inputValue: (this.state.inputValue * 10) + number,
      input: this.state.input + number,
      canUseOperand: true
    });
  }

  clickOnPriorizedOperand (operand, operandFunc) {
    if (!this.state.canUseOperand) return;

    const priorizedTotal = this.computePriorizedTotal();
    this.setState({
      priorizedComputedValue: priorizedTotal,
      currentOperandFunc: operandFunc,
      inputValue: null,
      input: this.state.input + operand,
      canUseOperand: false
    });
  }

  clickOnSimpleOperand (operand, operandFunc) {
    if (!this.state.canUseOperand) return;

    const total = this.computeTotal();
    this.setState({
      currentOperandFunc: operandFunc,
      priorizedComputedValue: null,
      computedValue: total,
      inputValue: null,
      input: this.state.input + operand,
      canUseOperand: false
    });
  }

  computePriorizedTotal () {
    const { computedPriorizedValue, inputValue, currentOperandFunc } = this.state;
    if (!currentOperandFunc) {
      return inputValue;
    }

    if (!computedPriorizedValue) {
      return inputValue;
    }

    if (!inputValue) {
      return computedPriorizedValue;
    }

    return currentOperandFunc(computedPriorizedValue, inputValue);
  }

  computeTotal () {
    const { computedValue, inputValue, currentOperandFunc } = this.state;
    if (!currentOperandFunc) {
      return inputValue;
    }

    if (!computedValue) {
      return inputValue;
    }

    if (!inputValue) {
      return computedValue;
    }

    return currentOperandFunc(computedValue, inputValue);
  }

  render () {
    const total = this.computeTotal();
    return (
      <div>
        <h1>Calculator</h1>
        <Screen text={this.state.input} total={total} />
        <div>
          <Button text='Mc' />
          <Button text='M+' />
          <Button text='M-' />
          <Button text='Mr' />
        </div>
        <div>
          <Button text='C' />
          <Button text='/' />
          <Button text='*' onClick={() => this.clickOnPriorizedOperand('*', computer.multiply)} />
          <Button text='<-' />
        </div>
        <div>
          <Button text='7' onClick={() => this.clickOnNumber(7)} />
          <Button text='8' onClick={() => this.clickOnNumber(8)} />
          <Button text='9' onClick={() => this.clickOnNumber(9)} />
          <Button text='-' onClick={() => this.clickOnSimpleOperand('-', computer.substract)} />
        </div>
        <div>
          <Button text='4' onClick={() => this.clickOnNumber(4)} />
          <Button text='5' onClick={() => this.clickOnNumber(5)} />
          <Button text='6' onClick={() => this.clickOnNumber(6)} />
          <Button text='+' onClick={() => this.clickOnSimpleOperand('+', computer.add)} />
        </div>
        <div>
          <Button text='1' onClick={() => this.clickOnNumber(1)} />
          <Button text='2' onClick={() => this.clickOnNumber(2)} />
          <Button text='3' onClick={() => this.clickOnNumber(3)} />
        </div>
        <div>
          <Button text='%' />
          <Button text='0' onClick={() => this.clickOnNumber(0)} />
          <Button text='.' />
          <Button text='=' />
        </div>
      </div>
    );
  }
}

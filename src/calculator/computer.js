export function add (number1, number2) {
  return number1 + number2;
}

export function substract (number1, number2) {
  return number1 - number2;
}

export function multiply (number1, number2) {
  return number1 * number2;
}
